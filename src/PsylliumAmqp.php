<?php

declare(strict_types=1);

namespace Psyllium\Amqp;

use Psyllium\Amqp\DependencyInjection\PsylliumAmqpExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class PsylliumAmqp extends AbstractBundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        return new PsylliumAmqpExtension();
    }
}
