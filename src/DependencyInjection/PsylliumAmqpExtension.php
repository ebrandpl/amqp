<?php

declare(strict_types=1);

namespace Psyllium\Amqp\DependencyInjection;

use PhpAmqpLib\Channel\AMQPChannel;
use Psyllium\Amqp\Infrastructure\RabbitMQ\ConnectionFactory;
use Psyllium\Amqp\Infrastructure\RabbitMQ\ListenerFactory;
use Psyllium\Amqp\Infrastructure\RabbitMQ\ProducerFactory;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class PsylliumAmqpExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        if ($config['rabbitmq']) {
            $container->register('psyllium.rabbitmq.channel', AMQPChannel::class)
                ->setFactory([ConnectionFactory::class, 'create'])
                ->setArguments([
                    $config['rabbitmq']['hostname'],
                    $config['rabbitmq']['port'],
                    $config['rabbitmq']['user'],
                    $config['rabbitmq']['pass']
                ]);
            $container->register('Psyllium\Amqp\Infrastructure\RabbitMQ\ListenerFactory', ListenerFactory::class)
                ->setArguments([
                    $container->getDefinition('psyllium.rabbitmq.channel')
                ]);
            $container->register('Psyllium\Amqp\Infrastructure\RabbitMQ\ProducerFactory', ProducerFactory::class)
                ->setArguments([
                    $container->getDefinition('psyllium.rabbitmq.channel')
                ]);
        }
    }
}