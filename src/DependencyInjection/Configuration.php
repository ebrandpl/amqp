<?php

declare(strict_types=1);

namespace Psyllium\Amqp\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('psyllium_amqp');
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('rabbitmq')
                ->children()
                    ->scalarNode('hostname')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->integerNode('port')
                        ->isRequired()
                    ->end()
                    ->scalarNode('user')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->scalarNode('pass')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                ->end()
            ->end()
        ->end();
        return $treeBuilder;
    }
}
