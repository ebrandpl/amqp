<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Domain\EventModel;

interface EventInterface
{
    public function getBody(): string;

    public function getHeaders(): array;

    public function getRouting(): string;

    public function getPriority(): int;
}
