<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Infrastructure\RabbitMQ;

use Psyllium\Amqp\Domain\EventModel\EventInterface;

interface ProducerInterface
{
    public function publish(EventInterface $event): void;
}
