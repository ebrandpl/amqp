<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Infrastructure\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;

class ListenerFactory
{
    public function __construct(
        protected readonly AMQPChannel $channel
    ) {
    }

    public function create(
        string $queue
    ): ListenerInterface {
        return new Listener(
            $this->channel,
            $queue
        );
    }
}
