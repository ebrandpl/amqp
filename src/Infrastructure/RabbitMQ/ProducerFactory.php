<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Infrastructure\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use Psr\Log\LoggerInterface;

class ProducerFactory
{
    public function __construct(
        protected readonly AMQPChannel $channel
    ) {
    }

    public function create(
        string $exchange
    ): ProducerInterface {
        return new Producer(
            $this->channel,
            $exchange
        );
    }
}
