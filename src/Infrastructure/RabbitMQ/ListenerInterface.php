<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Infrastructure\RabbitMQ;

interface ListenerInterface
{
    public function listen(callable $callback): void;
}
