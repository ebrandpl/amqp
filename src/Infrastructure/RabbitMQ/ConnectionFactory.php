<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Infrastructure\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ConnectionFactory
{
    public static function create(
        string $host,
        int $port,
        string $user,
        string $password
    ): AMQPChannel {
        $channel = (new AMQPStreamConnection($host, $port, $user, $password))->channel();
        $channel->basic_qos(0, 1, false);
        return $channel;
    }
}
