<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Infrastructure\RabbitMQ;

use Psyllium\Amqp\Domain\EventModel\EventInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class Producer implements ProducerInterface
{
    public function __construct(
        protected readonly AMQPChannel $channel,
        protected readonly string $exchange
    ) {
    }

    public function publish(EventInterface $event): void
    {
        $this->channel->basic_publish(
            msg: new AMQPMessage(
                $event->getBody(),
                [
                    'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                    'application_headers' => new AMQPTable($event->getHeaders()),
                    'priority' => $event->getPriority()
                ]
            ),
            exchange: $this->exchange,
            routing_key: $event->getRouting()
        );
    }
}
