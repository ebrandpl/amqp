<?php

declare(strict_types=1);

namespace Psyllium\Amqp\Infrastructure\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;

class Listener implements ListenerInterface
{
    public function __construct(
        protected readonly AMQPChannel $channel,
        protected readonly string $queue
    ) {
    }

    public function listen(callable $callback): void
    {
        $this->channel->basic_consume(
            queue: $this->queue,
            callback: $callback
        );
        while ($this->channel->is_consuming()) {
            $this->channel->wait(
                timeout: 3600
            );
        }
    }
}
